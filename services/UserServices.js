const jwt = require('jsonwebtoken');
const { User } = require('../models');
const config = require('../config');
const RemidlyError = require('../errors/RemidlyError');

module.exports = {
  createUser: (body) => User.create(body),

  findAllUsers: () => User.find().exec()
    .then((Users) => Users)
    .catch((err) => { throw new RemidlyError(err); }),

  updateUserById: (idUser, update) => User.findOneAndUpdate(
    { _id: idUser }, update, { new: true },
  ).exec()
    .then((User) => User)
    .catch((err) => { throw new RemidlyError(err); }),

  generateUserToken: (User) => {
    const newUser = {
      // eslint-disable-next-line no-underscore-dangle
      id: User._id,
      role: User.role,
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24),
    };
    return jwt.sign(newUser, config.SECRET_KEY);
  },

  findUserById: (id) => User.findOne({ _id: id, isActive: true })
    .populate('users.user')
    .exec()
    .then((User) => User)
    .catch((err) => {
      throw new RemidlyError(err);
    }),

  findUserByEmail: (email) => User.findOne({ email, isActive: true }).exec()
    .then((User) => User).catch(() => false),

  validatePassword: (User, password) => new Promise((resolve, reject) => {
    User.comparePassword(password, (err, isMatch) => {
      if (err) reject(err);
      return resolve(isMatch);
    });
  }),

  addStudent: (idStudent, idUser) => User.findOneAndUpdate({ _id: idUser },
    { $push: { students: { student: idStudent } } }, { new: true })
    .exec()
    .then((User) => User)
    .catch((err) => { throw new RemidlyError(err); }),
};
