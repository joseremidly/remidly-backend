const { User } = require('../models');
const RemidlyError = require('../errors/RemidlyError');

module.exports = {
  createUser: (body) => User.create(body),

  findAllUsers: () => User.find().exec()
    .then((Users) => Users)
    .catch((err) => { throw new RemidlyError(err); }),

  updateUserById: (idUser, update) => User.findOneAndUpdate(
    { _id: idUser }, update, { new: true },
  ).exec()
    .then((User) => User)
    .catch((err) => { throw new RemidlyError(err); }),
};
