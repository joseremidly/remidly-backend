const { User } = require('../models');
const RemidlyError = require('../errors/RemidlyError');
var request = require('request');

module.exports = {
  getPaymentMethods: (countryCode) => {
    const headersRapyd = {
      'content-type': 'application/json',
      'access_key': process.env.RAPYD_ACCESS_KEY
    }
    request('https://sandboxapi.rapyd.net/v1/payment_methods/country?country=' + countryCode, headers=headersRapyd, (error, response, body) => {
      console.log(response)
      if (!error && response.statusCode == 200) {
        console.log(body); // Print the google web page.
        return response;
      }
      return true;
    });
  },
};
