const UserServices = require('./UserServices');
const RapydServices = require('./RapydServices');

module.exports = {
  UserServices,
  RapydServices,
};
