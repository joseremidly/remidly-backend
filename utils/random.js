module.exports = {
  getRandomCode: () => {
    const randomCode = Math.floor(Math.random() * 9999) + 1
    const randomCodeString = randomCode.toString()
    if (randomCodeString.length === 1) {
      return '000' + randomCodeString
    } else if (randomCodeString.length === 2) {
      return '00' + randomCodeString
    } else if (randomCodeString.length === 3) {
      return '0' + randomCodeString
    } else {
      return randomCodeString
    }
  }
}
