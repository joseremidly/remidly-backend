const sgMail = require('@sendgrid/mail')
const { emailPreRegister } = require('./templates/emailPreRegister');
const CryptoJS = require("crypto-js");

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

module.exports = {
  sendEmailPreRegister: (email) => {
    let emailHash = CryptoJS.AES.encrypt(email, process.env.SECRET_KEY).toString()
    const msg = emailPreRegister(email, emailHash)
    return sgMail.send(msg)
      .then(() => {
        console.log('Email sent')
      })
      .catch((error) => {
        console.error(error)
      })
  },
}
