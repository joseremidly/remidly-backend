module.exports = {
  emailPreRegister: (email, emailHash) => {
    const buttonLink = `${process.env.URL_HOME}/verify/?email=${emailHash}`
    return {
      to: email,
      from: {
        email: 'hola@remidly.io',
        name: 'Remidly'
      },
      subject: 'Bienvenid@ a Remidly',
      html: ``,
    }
  },
}
