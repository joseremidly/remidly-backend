module.exports = {
  ENUM_GENDER: ['M', 'F', 'X'],
  ENUM_PLAN: ['FREE', 'ELITE'],
  ENUM_ROLE: ['ADMIN', 'USER'],
};
