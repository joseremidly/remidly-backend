const express = require('express');

const router = express.Router();
const { UserValidator } = require('../validators');
const { SignupUserController } = require('../controllers');
const { UserServices } = require('../services');
const asynError = require('../errors/asyncErrors');
const RemidlyError = require('../errors/RemidlyError');

router.post('/user', [UserValidator.createUserValidator], SignupUserController.create(UserServices, asynError));
router.post('/user/login', [UserValidator.loginValidator], SignupUserController.login(UserServices, asynError, RemidlyError));

module.exports = router;
