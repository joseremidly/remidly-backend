const express = require('express');

const router = express.Router();
// const permit = require('../utils/permit');
const { RapydController } = require('../controllers');
const { RapydServices } = require('../services');
const asynError = require('../errors/asyncErrors');

router.get('/rapyd/payments-methods/:countryCode', RapydController.getPaymentsMethods(RapydServices, asynError));
// router.post('/calculator/calculate', RapydController.calculate(Prices, asynError));

module.exports = router;
