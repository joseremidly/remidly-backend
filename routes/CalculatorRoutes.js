const express = require('express');

const router = express.Router();
// const permit = require('../utils/permit');
const { CalculatorController } = require('../controllers');
// const { CalculatorServices } = require('../services');
const Prices = require('../utils/prices');
const asynError = require('../errors/asyncErrors');

router.get('/calculator/prices', CalculatorController.getPrices(Prices, asynError));
router.post('/calculator/calculate', CalculatorController.calculate(Prices, asynError));

module.exports = router;
