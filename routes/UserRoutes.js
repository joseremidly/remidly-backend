const express = require('express');

const router = express.Router();
const permit = require('../utils/permit');
// const { UserMiddlewares } = require('../middlewares');
// const { UserValidator } = require('../validators');
const { UserController } = require('../controllers');
const { UserServices } = require('../services');
const asynError = require('../errors/asyncErrors');

router.get('/user/me', permit('USER'), UserController.me(UserServices, asynError));
// router.get('/user/:idUser', permit('ADMIN'), [StudentMiddlewares.isStudentById(StudentService)], StudentController.findById());
// router.patch('/user/:idUser', permit('ADMIN'), [StudentValidator.updateParentValidator, StudentMiddlewares.isStudentById(StudentService)], StudentController.update(StudentService, asynError));

module.exports = router;
