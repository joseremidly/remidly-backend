const express = require('express');

const router = express.Router();

// Signup User
router.use(require('./SignupUserRoutes'));

// Calculator User
router.use(require('./CalculatorRoutes'));

// Rapyd User
router.use(require('./RapydRoutes'));

module.exports = router;
