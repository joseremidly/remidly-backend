const { celebrate, Joi } = require('celebrate');
const { ENUM_ROLE, ENUM_GENDER, ENUM_PLAN } = require('../utils/const');

module.exports = {
  createUserValidator: celebrate({
    body: Joi.object().keys({
      name: Joi.string().required(),
      lastName: Joi.string().required(),
      birthday: Joi.string().optional(),
      gender: Joi.string().valid(ENUM_GENDER).optional(),
      phone: Joi.string().optional(),
      location: Joi.string().optional(),
      email: Joi.string().required(),
      password: Joi.string().min(6).required(),
      role: Joi.string().valid(ENUM_ROLE).required(),
    }),
  }),
  updateUserValidator: celebrate({
    body: Joi.object().keys({
      name: Joi.string().optional(),
      lastName: Joi.string().optional(),
      birthday: Joi.string().optional(),
      gender: Joi.string().valid(ENUM_GENDER).optional(),
    }),
  }),
  loginValidator: celebrate({
    body: Joi.object().keys({
      email: Joi.string().required(),
      password: Joi.string().required(),
    }),
  }),
};
