const RemidlyError = require('../errors/RemidlyError');

module.exports = {
  isParentById: (parentService) => async (req, res, next) => {
    const { idParent } = req.params;
    const parent = await parentService.findParentById(idParent);
    if (parent) {
      req.parent = parent;
      return next();
    }
    return next(new RemidlyError('No existe el Parent'));
  },
};
