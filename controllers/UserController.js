module.exports = {
  find: (userService, asyncError) => asyncError(async (req, res) => {
    const users = await userService.findAllUsers();
    return res.status(200).json(users);
  }),

  findById: () => (req, res) => res.status(200).json(req.user),

  update: (userService, asyncError) => asyncError(async (req, res) => {
    const userUpdated = await (
      await userService.updateUserById(req.params.idUser, req.body)
    ).toObject();
    delete userUpdated.password;
    return res.status(200).json(userUpdated);
  }),

  me: (userService, asyncError) => asyncError(async (req, res) => {
    const user = await (
      await userService.findUserById(req.decoded.id)
    ).toObject();
    delete user.password;
    res.send({ user, role: req.decoded.role });
  }),
};
