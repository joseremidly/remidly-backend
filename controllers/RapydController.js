module.exports = {
  getPaymentsMethods: (rapydService, asyncError) => asyncError(async (req, res) => {
    const methods = await rapydService.getPaymentMethods(req.params.countryCode);
    return res.status(200).json(methods);
  }),
};
