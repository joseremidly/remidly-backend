module.exports = {
  getPrices: (pricesService, asyncError) => asyncError(async (req, res) => {
    const prices = await pricesService.getPrices();
    return res.status(200).json(prices);
  }),
  calculate: (pricesService, asyncError) => asyncError(async (req, res) => {
    const prices = await pricesService.getPrices();
    // 5% fee estimated
    const moneyToSend = prices.mxn * req.body.amount * .95;
    return res.status(201).json({ total: moneyToSend });
  }),
};
