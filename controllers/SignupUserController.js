module.exports = {
  create: (userService, asyncError) => asyncError(async (req, res) => {
    const newUser = await userService.createUser(req.body);
    if (newUser) {
      const token = userService.generateUserToken(newUser);
      return res.status(200).send({ token, role: newUser.role });
    }
    return res.status(200).json(newUser);
  }),

  login: (userService, asyncError, RemidlyError) => asyncError(async (req, res, next) => {
    const user = await userService.findUserByEmail(req.body.email);
    if (user) {
      const isMatch = await userService.validatePassword(user, req.body.password);
      if (isMatch) {
        const token = userService.generateUserToken(user);
        return res.status(200).send({ token, role: user.role });
      }
      return next(new RemidlyError('Password incorrecto', 400));
    }
    return next(new RemidlyError('No existe el email', 400));
  }),
};
