/* eslint-disable func-names */
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const {
  ENUM_GENDER,
  ENUM_ROLES,
} = require('../utils/const');

const SALT_WORK_FACTOR = 10;

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  birthday: {
    type: String,
  },
  gender: {
    type: String,
    enum: ENUM_GENDER,
  },
  phone: {
    type: String,
    required: true,
  },
  location: {
    type: String,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  transactions: {
    type: [
      {
        transaction: {
          type: mongoose.Types.ObjectId,
          ref: 'transaction',
        },
      },
    ],
  },
  role: {
    type: String,
    enum: ENUM_ROLES,
    required: true,
    default: 'USER',
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  isVerify: {
    type: Boolean,
    default: false,
  },
}, { timestamps: true });

userSchema.pre('save', function (next) {
  const user = this;
  if (!user.isModified('password')) return next();
  bcrypt.genSalt(SALT_WORK_FACTOR, (errGenSalt, salt) => {
    if (errGenSalt) return next(errGenSalt);
    bcrypt.hash(user.password, salt, (errHash, hash) => {
      if (errHash) return next(errHash);
      user.password = hash;
      next();
      return true;
    });
    return true;
  });
  return true;
});

userSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) return cb(err);
    cb(null, isMatch);
    return true;
  });
};

module.exports = mongoose.model('user', userSchema);
